package com.classpathio.s3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwsJavaSdkExamplesApplication {

	public static void main(String[] args) {
		SpringApplication.run(AwsJavaSdkExamplesApplication.class, args);
	}

}
